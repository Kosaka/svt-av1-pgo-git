#!/hint/bash
# Maintainer : Kosaka <kosaka@noreply.codeberg.org>
# Contributor: Maxime Gauduin <alucryd@archlinux.org>
# Contributor: Daniel Bermond <dbermond@archlinux.org>
# Contributor: Thomas Schneider <maxmusterm@gmail.com>
# shellcheck disable=SC2034,SC2154

pkgname=svt-av1-psy-pgo
pkgver=v2.3.0.r0.g0fb626b
pkgrel=1
pkgdesc='Scalable Video Technology AV1 encoder and decoder'
arch=(x86_64)
url='https://github.com/gianni-rosato/svt-av1-psy'
provides=('svt-av1')
conflicts=('svt-av1')
license=(
    BSD
    'custom: Alliance for Open Media Patent License 1.0'
)
depends=(glibc)
makedepends=(
    av1an
    clang
    cmake
    git
    llvm
    nasm
    ninja
    python
)
source=('git+https://github.com/gianni-rosato/svt-av1-psy'
        'encode.py'
        'encode_settings.conf')
b2sums=('SKIP'
        'SKIP'
        'SKIP')
_where="$PWD"
_repo="svt-av1-psy"

#*OPTIONS
# Enable Bolting of SVT-AV1 for slightly better performance. Requires LLVM built with Bolt enabled to be in $PATH
# E.g the programs "llvm-bolt", and "merge-fdata" along with their required libraries.
BOLT=true

# Automatic input video download/usage. Only one can be used at a time.
# Downloads Xiph's test video archives which contain many small clips used for PGO optimisation. If disabled, you have to provide *all* of the input videos yourself.
# However, you can still provide your own videos if you use one of these options.
# The options are:
#   - objective-1-fast: roughly 3.5 gigabytes uncompressed. Recommended to use this if you don't want to provide your own.
#   - objective-1: roughly 26-27 gigabytes uncompressed.
#   - objective-2-fast: roughly 4.5 gigabytes uncompressed.
#   - objective-2-slow: roughly 21 gigabytes uncompressed.
#   - none: No video will be downloaded. You have to provide your own.
DOWNLOAD_OBJECTIVE_TYPE="objective-1-fast"

#*END OF OPTIONS

if test "$BOLT" == "true"; then
    pkgname="$pkgname-bolt-git"
    # Bolted binaries cannot be stripped (yet).
    options=('!strip')
else
    pkgname="$pkgname-git"
fi

# Add the selected tar file to the source array.
if test "$DOWNLOAD_OBJECTIVE_TYPE" != "none"; then
    source+=("https://media.xiph.org/video/derf/${DOWNLOAD_OBJECTIVE_TYPE}.tar.gz")
    b2sums+=('SKIP')
fi

# Colourful colours
if ! test "$NO_COLOR"; then
    red="\e[1;31m"
    nc="\e[0m"
fi

prepare() {
    # Check for llvm-bolt if the user is Bolting.
    if test "$BOLT" == "true"; then
        _programs=(llvm-bolt merge-fdata)

        # Check to see if _programs exists
        for _program in "${_programs[@]}"; do
            if ! command -v "$_program"; then
                echo -e "${red}[ERROR] $_program is not installed! Please install and add it to your \$PATH before running this PKGBUILD${nc}"
                exit 1
            fi
        done

        # Create the svt-bolt-data folder if missing.
        if ! test -d "${srcdir}"/svt-bolt-data; then
            if ! mkdir "${srcdir}"/svt-bolt-data; then
                # Exit if the creation of the svt-bolt-data folder fails.
                echo -e "${red}[ERROR] Failed to create svt-bolt-data folder${nc}"
                exit 1
            fi
        fi
    fi

    # Check for the video-input folder.
    if test -d "$_where"/video-input; then
        # Symlink the video-input folder into the srcdir
        if ! test -d "${srcdir}"/video-input; then
            if ! ln -s "$_where"/video-input "${srcdir}"/video-input; then
                # Exit if the creation of the symlink fails.
                echo -e "${red}[ERROR] Failed to symlink video-input folder into \$SRCDIR${nc}"
                exit 1
            fi
        fi
    fi
}


pkgver() {
    cd "$_repo" || exit 1
    git describe --long --tags --abbrev=7 | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
}

build() {
    export LDFLAGS="$LDFLAGS -Wl,-z,noexecstack"
    # Bolt requires linking with relocs
    if test "$BOLT" == "true"; then
        export LDFLAGS="$LDFLAGS -Wl,--emit-relocs"
    fi

    # PGO requires using Clang.
    export CC=clang
    export CXX=clang++

    # make it so that the PID is added to the profraw file.
    export LLVM_PROFILE_FILE="build/%p-svt.profraw"

    # Build SVT-AV1 to generate our PGO data.
    cmake -S "$_repo" -B build -G "Unix Makefiles" \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DBUILD_SHARED_LIBS=ON \
        -DSVT_AV1_PGO=ON \
        -DENABLE_AVX512=ON \
        -DCMAKE_BUILD_TYPE=Release \
        -DSVT_AV1_LTO=ON \
        -DNATIVE=OFF
    make PGOCompileGen -C build
    # cmake --build build --target PGOCompileGen

    # Generate our PGO data by encoding various test videos.
    # The user can supply their own settings in the encode_settings.conf file.
    # Custom videos can be put into the video-input folder.
    python "${srcdir}"/encode.py --svt-repo "${_repo}"

    # remove .av1an files as we do not need them.
    if test -d "${srcdir}"/video-input; then
        rm "${srcdir}"/video-input/*.av1an -f
    elif test -d "${srcdir}/$DOWNLOAD_OBJECTIVE_TYPE"; then
        rm "${srcdir}"/$DOWNLOAD_OBJECTIVE_TYPE/*.av1an -f
    fi

    # Merge the generated data into something useable.
    # llvm-profdata merge "${srcdir}/svt-pgo-data"/*.profraw-real --output "${srcdir}"/svt-pgo-data/default.profdata

    if test "$BOLT" == "true"; then
        # Compile SVT-AV1 using our new PGO data.
        make PGOCompileUse -C build

        # Use Bolt on SVT-AV1 to generate profile data. This is different from PGO and of course more confusing.
        mv "$PWD/$_repo/Bin/Release/SvtAv1EncApp" "$PWD/$_repo/Bin/Release/non-bolt-SvtAv1EncApp"
        llvm-bolt "$PWD/$_repo/Bin/Release/non-bolt-SvtAv1EncApp" \
            --instrument \
            --instrumentation-file-append-pid \
            --instrumentation-file="${srcdir}"/svt-bolt-data/svt-data.fdata \
            -o "$PWD/$_repo/Bin/Release/SvtAv1EncApp"

        # Do more encoding to generate Bolt data
        ./encode.py --svt-repo "$_repo"

        # remove .av1an files as we do not need them.
        if test -d "${srcdir}"/video-input; then
            rm "${srcdir}"/video-input/*.av1an -f
        elif test -d "${srcdir}/objective-*"; then
            rm "${srcdir}"/objective-*/*.av1an -f
        fi

        # compile all of our fdata files into one
        merge-fdata "${srcdir}/svt-bolt-data"/*.fdata > "${srcdir}/svt-bolt-data/final.fdata-real"

        # Finally Bolt on our generated data to the SVT binary using llvm-bolt.
        mv "$PWD/$_repo/Bin/Release/SvtAv1EncApp" "$PWD/$_repo/Bin/Release/pre-bolt-SvtAv1EncApp"
        llvm-bolt "$PWD/$_repo/Bin/Release/non-bolt-SvtAv1EncApp" \
            --data="${srcdir}/svt-bolt-data/final.fdata-real" \
            -reorder-blocks=ext-tsp \
            -reorder-functions=hfsort+ \
            -split-functions \
            -split-all-cold \
            -split-eh \
            -dyno-stats \
            -icf=1 \
            -use-gnu-stack \
            -plt=hot \
            -o "$PWD/$_repo/Bin/Release/SvtAv1EncApp"
            # -icp-eliminate-loads \
            # -indirect-call-promotion=all \
            # -jump-tables=basic \
            # -align-macro-fusion=hot \
    fi
}

package() {
    if test "$BOLT" == "true"; then
        DESTDIR="${pkgdir}" cmake --install build
    else
        DESTDIR="${pkgdir}" ninja -C build install
    fi
    install -Dm 644 "$_repo"/{LICENSE,PATENTS}.md -t "${pkgdir}"/usr/share/licenses/svt-av1/
}

# vim: ts=2 sw=2 et:
